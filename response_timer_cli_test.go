package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestParseFlags(t *testing.T) {
	// Valid url, period and frequency
	URL, period, frequency, err := parseFlags("http://gitlab.com", "5m", "10s")
	assert.Nil(t, err)
	assert.Equal(t, 10*time.Second, frequency)
	assert.Equal(t, 5*time.Minute, period)
	assert.Equal(t, "http://gitlab.com", URL.String())

	// Period and frequency should be positive
	URL, period, frequency, err = parseFlags("http://gitlab.com", "-5m", "10s")
	assert.NotNil(t, err)

	URL, period, frequency, err = parseFlags("http://gitlab.com", "5m", "-10s")
	assert.NotNil(t, err)

	// period and frequency cannot be 0
	URL, period, frequency, err = parseFlags("http://gitlab.com", "0", "10s")
	assert.NotNil(t, err)

	URL, period, frequency, err = parseFlags("http://gitlab.com", "5m", "0")
	assert.NotNil(t, err)

	// url should be absolute
	URL, period, frequency, err = parseFlags("gitlab.com", "5m", "10s")
	assert.NotNil(t, err)

}
