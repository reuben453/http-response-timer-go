package responsetimer

import (
	"net/http"
	"sync"
	"time"
)

// RequestHop represents a single request to a URL and the time taken.
type RequestHop struct {
	URL      string
	Duration time.Duration
}

// CustomTransport implements the http.RoundTripper interface
type CustomTransport struct {
	rtp http.RoundTripper

	requestHops []RequestHop
	sync.RWMutex
}

// NewTransport return an instance of the CustomTransport object
func NewTransport() *CustomTransport {
	tr := &CustomTransport{}

	tr.rtp = &http.Transport{
		Proxy:             http.ProxyFromEnvironment,
		DisableKeepAlives: true,
	}

	return tr
}

// RequestHops returns the requestHops array
func (tr *CustomTransport) RequestHops() []RequestHop {
	tr.RLock()
	defer tr.RUnlock()

	return tr.requestHops
}

// ResetRequestHops sets the requestHops array to nil
func (tr *CustomTransport) ResetRequestHops() {
	tr.Lock()
	defer tr.Unlock()

	tr.requestHops = nil
}

// RoundTrip calls the default http.RoundTripper and records the time taken to execute
func (tr *CustomTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	reqStart := time.Now()
	resp, err := tr.rtp.RoundTrip(r)
	reqEnd := time.Now()
	currentDuration := reqEnd.Sub(reqStart)

	hop := RequestHop{
		URL:      r.URL.String(),
		Duration: currentDuration,
	}

	tr.Lock()
	defer tr.Unlock()
	tr.requestHops = append(tr.requestHops, hop)

	return resp, err
}
