// responsetimer can be used to get the response time for each round trip or hop made when
// requesting a url.

package responsetimer

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"time"
)

// GetResponseTime makes a request for the given url with the given timeout and returns a RequestHop array
// containing the hops made while getting the url.
func GetResponseTime(url string, timeout time.Duration) ([]RequestHop, error) {
	tp := NewTransport()
	client := &http.Client{
		Transport: tp,
		Timeout:   timeout,
	}

	log.Printf("Making a request to %v", url)
	resp, err := client.Get(url)

	if err != nil {
		log.Printf("get error: %s: %s", err, url)
		return nil, err
	}
	defer resp.Body.Close()

	io.Copy(ioutil.Discard, resp.Body)

	return tp.RequestHops(), nil
}

// GetMultipleResponseTimes gets the response time for the given url at regular intervals defined by the
// frequency parameter for a time period defined by the period parameter.
// If frequency is 10 seconds and period is 5 minutes, this function will get the response time
// every 10 seconds for 5 minutes.
func GetMultipleResponseTimes(url string, frequency time.Duration, period time.Duration) ([][]RequestHop, []error) {

	var allRequestHops [][]RequestHop
	var errs []error

	// Make first request
	requestHops, err := GetResponseTime(url, frequency)
	if err == nil {
		allRequestHops = append(allRequestHops, requestHops)
	} else {
		errs = append(errs, err)
	}

	ticker := time.NewTicker(frequency)
	stopWatch := time.NewTimer(period)

foreverLoop:
	for {

		select {
		case <-ticker.C:
			requestHops, err := GetResponseTime(url, frequency)
			if err == nil {
				allRequestHops = append(allRequestHops, requestHops)
			} else {
				errs = append(errs, err)
			}

		case <-stopWatch.C:
			ticker.Stop()
			break foreverLoop
		}

	}

	return allRequestHops, errs

}

// CalculateAverageResponseTimes will return the average response time for each request hop.
// Each array of RequestHop represents the hops done to fulfill one request.
// The 2d array of RequestHop represents all the requests made.
func CalculateAverageResponseTimes(allRequestHops [][]RequestHop) []RequestHop {
	type ResponseTimeSum struct {
		// count is the number of response times that have been added to responseTimeSum.
		count int

		// responseTimeSum is the sum of all response times for a particular url.
		responseTimeSum time.Duration

		// index is used so that we know the original order of the hops since maps do not retain the order.
		index int
	}

	// Use a map to sum up the response times for each url.
	averageResponseTimes := make(map[string]ResponseTimeSum)
	for _, requestHops := range allRequestHops {
		for requestHopIndex, requestHop := range requestHops {

			val, present := averageResponseTimes[requestHop.URL]

			// Set the index if this is the first time we are encountering this url.
			if !present {
				val.index = requestHopIndex + 1
			}

			val.count++
			val.responseTimeSum += requestHop.Duration

			averageResponseTimes[requestHop.URL] = val
		}
	}

	// Put the response time of each hop present in the map into a RequestHop array.
	var averageRequestHops []RequestHop
	for url, val := range averageResponseTimes {
		averageRequestHops = append(averageRequestHops, RequestHop{
			URL:      url,
			Duration: val.responseTimeSum / time.Duration(val.count),
		})
	}

	// sort the array in the original order of hops.
	less := func(i, j int) bool {
		valI, present := averageResponseTimes[averageRequestHops[i].URL]
		if !present {
			// This should never happen since the array values are taken from the map.
			log.Panicf("ith value not present in map! ithvalue: %+v, map: %v", averageRequestHops[i], averageResponseTimes)

		}

		valJ, present := averageResponseTimes[averageRequestHops[j].URL]
		if !present {
			// This should never happen since the array values are taken from the map.
			log.Panicf("ith value not present in map! jthvalue: %+v, map: %v", averageRequestHops[j], averageResponseTimes)

		}

		return valI.index < valJ.index
	}

	sort.SliceStable(averageRequestHops, less)

	return averageRequestHops
}

// GetAverageResponseTime will return all requests and the average response time for each request hop,
// along with an array of errors. The errors will be non-nil if there was any error while requesting
// the given url.
func GetAverageResponseTime(url string, frequency time.Duration, period time.Duration) ([][]RequestHop, []RequestHop, []error) {

	allRequestHops, errs := GetMultipleResponseTimes(url, frequency, period)

	return allRequestHops, CalculateAverageResponseTimes(allRequestHops), errs
}

// GetTotalResponseDuration returns the sum of response times of each request hop in the given array.
func GetTotalResponseDuration(requestHops []RequestHop) time.Duration {
	var sum time.Duration
	for _, requestHop := range requestHops {
		sum += requestHop.Duration
	}
	return sum
}
