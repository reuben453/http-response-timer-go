package responsetimer

import (
	"context"
	"fmt"
	"html"
	"log"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

const (
	port = ":8081"
	url  = "http://localhost" + port
)

// createServer will create an http server on the given port and return the server object.
// This function will only return when the server has successfully started.
func createServer(port string) *http.Server {
	serveMux := http.NewServeMux()

	serveMux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
	})

	server := &http.Server{
		Addr:    port,
		Handler: serveMux,
	}

	go func() {
		log.Println(server.ListenAndServe())
	}()

	// Check if the server is up
	client := &http.Client{}
	var err error
	_, err = client.Get(url)
	log.Println("starting check server for loop", err)

	// Loop until the server is ready
	for err != nil {
		time.Sleep(10 * time.Millisecond)
		_, err = client.Get(url)
		log.Println("Ran check server for loop", err)
	}

	return server

}

func TestGetResponseTime(t *testing.T) {
	server := createServer(port)
	defer server.Shutdown(context.Background())

	vals, err := GetResponseTime(url, 2*time.Second)
	assert.Nil(t, err)
	fmt.Println(vals)
}

func TestGetMultipleResponseTimes(t *testing.T) {
	server := createServer(port)
	defer server.Shutdown(context.Background())

	// Check that the correct number of requests are made
	vals, errs := GetMultipleResponseTimes(url, 1*time.Second, 1*time.Second)
	assert.Nil(t, errs)
	assert.Equal(t, 2, len(vals))

	vals, errs = GetMultipleResponseTimes(url, 1*time.Second, 3*time.Second)
	assert.Nil(t, errs)
	assert.Equal(t, 4, len(vals))

	// Check that errors are returned correctly
	vals, errs = GetMultipleResponseTimes("http://localhost:12345", 1*time.Second, 1*time.Second)
	assert.NotNil(t, errs)
	assert.Equal(t, 2, len(errs))
	assert.Equal(t, 0, len(vals))
}

func TestGetAverageResponseTime(t *testing.T) {
	server := createServer(port)
	defer server.Shutdown(context.Background())

	allRequestHops, averageRequestHops, errs := GetAverageResponseTime(url, 1*time.Second, 1*time.Second)
	assert.Nil(t, errs)
	assert.Equal(t, 2, len(allRequestHops))
	assert.Equal(t, 1, len(averageRequestHops))
}

func TestCalculateAverageResponseTimes(t *testing.T) {
	url1 := url
	url2 := "http://localhost:8081/hi/"
	responseTimes := [][]RequestHop{
		{
			{URL: url1, Duration: time.Second * 2},
			{URL: url2, Duration: time.Second * 4},
		}, {
			{URL: url1, Duration: time.Second * 3},
			{URL: url2, Duration: time.Second * 2},
		}, {
			{URL: url1, Duration: time.Second * 1},
			{URL: url2, Duration: time.Second * 1},
		},
	}

	averageResponseTimes := CalculateAverageResponseTimes(responseTimes)

	// There are 2 hops per request, so the average should also have 2 entries
	assert.Equal(t, 2, len(averageResponseTimes))

	assert.Equal(t, url1, averageResponseTimes[0].URL)
	assert.Equal(t, url2, averageResponseTimes[1].URL)

	assert.Equal(t, 2*time.Second, averageResponseTimes[0].Duration)
	assert.Equal(t, 2333333333*time.Nanosecond, averageResponseTimes[1].Duration)

}

func TestGetTotalResponseTime(t *testing.T) {
	url1 := url
	url2 := "http://localhost:123"
	responseTimes := []RequestHop{
		{URL: url1, Duration: time.Second * 2},
		{URL: url2, Duration: time.Second * 4},
	}
	totalResponseDuration := GetTotalResponseDuration(responseTimes)
	assert.Equal(t, 6*time.Second, totalResponseDuration)
}
