# http-response-timer-go

This tool gives you the average response time for a given url.
If there are redirects, the response time of every round-trip, along with the total response time, will be printed.

## Build

`dep ensure`

`go build response_timer_cli.go`

## Test

`go test -cover -race -timeout 30s ./...`

## Execute

`./response_timer_cli -period 5m -frequency 10s http://gitlab.com`

* -period is the time period over which the average should be calculated.
* -frequency is how often the response time should be checked within the above time period.

    Both frequency and period accept Golang duration strings. Ex: '5m' for 5 minutes, '10s' for 10 seconds, '1h' for 1 hour.

* The URL should be an absolute url, as shown in the above example.

### Example Output

`./response_timer_cli -period 20s -frequency 10s http://gitlab.com`

This makes 3 requests; first at 0s, second at 10s and third at 20s.

Output:

    URL: http://gitlab.com , frequency: 10s , period: 20s
    2018/05/02 22:23:21 Making a request to http://gitlab.com
    2018/05/02 22:23:34 Making a request to http://gitlab.com
    2018/05/02 22:23:44 Making a request to http://gitlab.com

    Request 1
    http://gitlab.com 558.797116ms
    https://gitlab.com/ 1.262035255s
    https://about.gitlab.com/ 1.571082822s
    Total response time: 3.391915193s

    Request 2
    http://gitlab.com 554.900693ms
    https://gitlab.com/ 1.086324786s
    https://about.gitlab.com/ 1.568422248s
    Total response time: 3.209647727s

    Request 3
    http://gitlab.com 554.847399ms
    https://gitlab.com/ 1.078083868s
    https://about.gitlab.com/ 1.569310346s
    Total response time: 3.202241613s

    Average response time:
    http://gitlab.com 556.181736ms
    https://gitlab.com/ 1.142147969s
    https://about.gitlab.com/ 1.569605138s
    Total average response time: 3.267934843s

There are 3 round-trips per request since http://gitlab.com redirects to https://gitlab.com which redirects to https://about.gitlab.com