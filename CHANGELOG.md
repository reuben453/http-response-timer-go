# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-05-02

CLI tool which provides the response time of a URL. If the URL redirects to another, the response times of both will be printed. Similarly, if there are multiple redirects, the response time of each round-trip will be printed along with the average of each round-trip and the overall average.