package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"net/url"
	"time"

	"gitlab.com/reuben453/http-response-timer-go/responsetimer"
)

// parseFlags parses the period and frequency strings into time.Duration objects.
func parseFlags(URL, periodStr, frequencyStr string) (parsedURL *url.URL, period, frequency time.Duration, err error) {

	parsedURL, err = url.ParseRequestURI(URL)
	if err != nil {
		return
	}

	period, err = time.ParseDuration(periodStr)
	if err != nil {
		fmt.Println("Invalid period. Period should be of the form: '10m' for 10 minutes, '200s' for 200 seconds")
		log.Printf("Invalid period duration string, err: %v, period_string: %v", err, periodStr)
		return
	} else if period <= 0 {
		err = errors.New("period should be greater than 0")
		log.Printf("Invalid period duration string, period_string: %v", periodStr)
		return
	}

	frequency, err = time.ParseDuration(frequencyStr)
	if err != nil {
		fmt.Println("Invalid frequency. Frequency should be of the form: '10s' for 10 seconds, '1m' for 1 minute")
		log.Printf("Invalid frequency duration string, err: %v, frequency_string: %v", err, frequencyStr)
		return
	} else if frequency <= 0 {
		err = errors.New("frequency should be greater than 0")
		log.Printf("Invalid frequency duration string, frequency_string: %v", frequencyStr)
		return
	}

	return
}

func printAllResponseTimes(allRequestHops [][]responsetimer.RequestHop) {
	for index, requestHops := range allRequestHops {
		fmt.Println("")
		fmt.Println("Request", index+1)

		// Print the response time for each hop in the request.
		for _, requestHop := range requestHops {
			fmt.Println(requestHop.URL, requestHop.Duration)
		}

		// Print the total response time of all roundtrips in the request
		fmt.Println("Total response time:", responsetimer.GetTotalResponseDuration(requestHops))
	}
}

func printResponseTimes(requestHops []responsetimer.RequestHop) {
	fmt.Println("Average response time:")
	for _, requestHop := range requestHops {
		// Print the average response time for each hop in the request
		fmt.Println(requestHop.URL, requestHop.Duration)
	}

	fmt.Println("Total average response time:", responsetimer.GetTotalResponseDuration(requestHops))
}

func main() {

	defer func() {
		if err := recover(); err != nil {
			log.Println("Fatal error. error:", err)
			fmt.Println("A fatal error has occurred. Check the logs for more details.")
			panic(err)
		}
	}()

	var periodStr, frequencyStr string

	// period flag specifies the time period over which you want an average.
	// The default is 5 minutes.
	flag.StringVar(&periodStr, "period", "5m", "Time period over which to get the response time to the given URL. Ex: '5m' for 5 minutes, '20s' for 20 seconds.")

	// frequency flag specifies how often response times should be checked within
	// the above time period. The default is 10 seconds.
	flag.StringVar(&frequencyStr, "frequency", "10s", "How often to check the response time. Ex: '5m' for 5 minutes, '20s' for 20 seconds.")
	flag.Parse()

	URLStr := flag.Args()[0]

	URL, period, frequency, err := parseFlags(URLStr, periodStr, frequencyStr)
	if err != nil {
		fmt.Println("Error parsing flags. Error:", err)
		return
	}

	fmt.Println("URL:", URL.String(), ", frequency:", frequency, ", period:", period)

	// Get the average response time for the requested url.
	allRequestHops, averageRequestHops, errs := responsetimer.GetAverageResponseTime(URL.String(), frequency, period)

	if errs != nil {
		fmt.Println("There were some errors. Check the log for more details.")
	}

	// Print the response times for all requests.
	printAllResponseTimes(allRequestHops)

	fmt.Println("")

	// Print the average response time for each hop and the overall average response time.
	printResponseTimes(averageRequestHops)

}
